// Copyright 2024 The libtcl9_0-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

// https://unix.stackexchange.com/questions/159193/unable-to-find-an-interpreter-when-running-a-windows-executable

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/adrg/xdg"
	"modernc.org/cc/v4"
	ccgo "modernc.org/ccgo/v4/lib"
	util "modernc.org/fileutil/ccgo"
)

const (
	version     = "9.0.1"
	archivePath = "tcl-core" + version + "-src.tar.gz"
	libraryGo   = `// Copyright 2024 The libtcl9_0-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package library provides the Tcl standard library.
package library // import "modernc.org/libtcl9_0/library"

import (
	_ "embed"
)

// Zip contains the Tcl standard library archive.
//
//go:embed library.zip
var Zip string
`
)

var (
	goarch        = env("TARGET_GOARCH", env("GOARCH", runtime.GOARCH))
	goos          = env("TARGET_GOOS", env("GOOS", runtime.GOOS))
	target        = fmt.Sprintf("%s/%s", goos, goarch)
	make          = "make"
	sed           = "sed"
	j             = fmt.Sprint(runtime.GOMAXPROCS(-1))
	xdgConfigHome = xdg.ConfigHome
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintln(os.Stderr, strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	if ccgo.IsExecEnv() {
		if err := ccgo.NewTask(goos, goarch, os.Args, os.Stdout, os.Stderr, nil).Main(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		return
	}

	dev := os.Getenv("GO_GENERATE_DEV") != ""
	f, err := os.Open(archivePath)
	if err != nil {
		fail(1, "cannot open tar file: %v\n", err)
	}

	_, extractedArchivePath := filepath.Split(archivePath)
	extractedArchivePath = extractedArchivePath[:len(extractedArchivePath)-len("-src.tar.gz")]
	xdgDir := filepath.Join(xdgConfigHome, "ccgo", "v4", "libtcl9.0", goos, goarch)
	libRoot := filepath.Join(xdgDir, "tcl"+version)
	makeRoot := filepath.Join(libRoot, "unix")
	os.MkdirAll(xdgDir, 0770)
	fmt.Fprintf(os.Stderr, "make %s\n", make)
	fmt.Fprintf(os.Stderr, "sed %s\n", sed)
	fmt.Fprintf(os.Stderr, "xdgDir %s\n", xdgDir)
	fmt.Fprintf(os.Stderr, "archivePath %s\n", archivePath)
	fmt.Fprintf(os.Stderr, "extractedArchivePath %s\n", extractedArchivePath)
	fmt.Fprintf(os.Stderr, "libRoot %s\n", libRoot)
	fmt.Fprintf(os.Stderr, "makeRoot %s\n", makeRoot)
	os.RemoveAll(filepath.Join("include", goos, goarch))
	os.Remove(filepath.Join("library"))
	os.Remove(filepath.Join("internal/tests"))
	os.RemoveAll(xdgDir)
	util.MustUntar(true, xdgDir, f, nil)
	fmt.Println("OK")
	util.MustShell(true, nil, sed, "-i", "s/#ifdef __i386/#if 0/", filepath.Join(libRoot, "generic", "tclStrToD.c"))
	mustCopyFile("LICENSE-TCL", filepath.Join(libRoot, "license.terms"), nil)
	if err != nil {
		fail(1, "%s\n", err)
	}

	result := "libtcl.a.go"
	cwd := util.MustAbsCwd(true)
	ilibz := filepath.Join(cwd, "..", "libz", "include", goos, goarch)
	util.MustInDir(true, makeRoot, func() (err error) {
		defer func() {
			if err != nil {
				trc("FAIL err=%v", err)
			}
			if e := recover(); e != nil {
				trc("PANIC: FAIL e=%v", e)
				panic(e)
			}
		}()
		var cflags []string
		cflags = []string{
			"-DNDEBUG",
			"-UHAVE_EVENTFD",
			"-UHAVE_FTS",
			"-UHAVE_SYS_EPOLL_H",
			"-UHAVE_SYS_EVENTFD_H",
			"-UHAVE_TERMIOS_H",
			"-UNOTIFIER_EPOLL",
			"-U__SIZEOF_INT128__",
		}
		if s := cc.LongDouble64Flag(goos, goarch); s != "" {
			cflags = append(cflags, s)
		}
		util.MustShell(true, nil, "sh", "-c", `
go mod init example.com/libtcl9.0
go get \
	modernc.org/libc@latest \
	modernc.org/libz@latest
`)
		if dev {
			util.MustShell(true, nil, "sh", "-c", `
go work init
go work use \
	$GOPATH/src/modernc.org/libc \
	$GOPATH/src/modernc.org/libz
`)
		}
		util.MustShell(true, nil, "sh", "-c", fmt.Sprintf("CFLAGS='%s' ./configure "+
			"--disable-dll-unloading "+
			"--disable-load "+
			"--disable-shared "+
			"--enable-64bit "+
			"", strings.Join(cflags, " ")))
		args := []string{os.Args[0]}
		if dev {
			args = append(
				args,
				"-absolute-paths",
				"-positions",
			)
		}
		args = append(args,
			"-hide", "TclGetCStackPtr",
			"-hide", "TclWinCPUID",
			"-hide", "TclpCreateProcess",
			"-hide", "__darwin_check_fd_set",
			"--prefix-external=X",
			"--prefix-field=F",
			"--prefix-static-internal=_",
			"--prefix-static-none=_",
			"--prefix-tagged-struct=T",
			"--prefix-tagged-union=T",
			"--prefix-typename=T",
			"--prefix-undefined=_",
			"-I", ilibz,
			"-eval-all-macros",
			"-extended-errors",
			"-ignore-link-errors",
			"-ignore-unsupported-alignment",
		)
		ccgo.NewTask(goos, goarch, append(args, "-exec", "make", "-j", j, "test-tcl"), os.Stdout, os.Stderr, nil).Exec()
		if err = ccgo.NewTask(goos, goarch, append(args, "-o", result, "--package-name", "libtcl9_0", "libtcl9.0.a", "-lz"), os.Stdout, os.Stderr, nil).Main(); err != nil {
			return err
		}

		return nil
	})

	mustCopyFile(filepath.Join("include", goos, goarch, "tcl.h"), filepath.Join(libRoot, "generic", "tcl.h"), nil)
	mustCopyFile(filepath.Join("include", goos, goarch, "tclDecls.h"), filepath.Join(libRoot, "generic", "tclDecls.h"), nil)
	mustCopyFile(filepath.Join("include", goos, goarch, "tclPlatDecls.h"), filepath.Join(libRoot, "generic", "tclPlatDecls.h"), nil)
	mustCopyDir("library", filepath.Join(libRoot, "library"), nil, false)
	if target == "linux/386" {
		util.MustInDir(true, libRoot, func() (err error) {
			_, err = util.Shell(nil, "zip", "-r", filepath.Join(cwd, "library", "library.zip"), "library")
			return err
		})
	}
	if err := os.WriteFile(filepath.Join("library", "library.go"), []byte(libraryGo), 0660); err != nil {
		fail(1, "%s\n", err)
	}

	mustCopyDir("internal/tests", filepath.Join(libRoot, "tests"), nil, false)
	fn := fmt.Sprintf("libtcl9_0_%s_%s.go", goos, goarch)
	mustCopyFile(fn, filepath.Join(makeRoot, result), nil)
	mustCopyFile(filepath.Join("internal", "tcltest", fn), filepath.Join(makeRoot, "tcltest.go"), nil)
	util.Shell(nil, "git", "status")
}

func mustCopyDir(dst, src string, canOverwrite func(fn string, fi os.FileInfo) bool, srcNotExistsOk bool) (files int, bytes int64) {
	file, bytes, err := copyDir(dst, src, canOverwrite, srcNotExistsOk)
	if err != nil {
		fail(1, "%s\n", err)
	}

	return file, bytes
}

func copyDir(dst, src string, canOverwrite func(fn string, fi os.FileInfo) bool, srcNotExistsOk bool) (files int, bytes int64, rerr error) {
	defer func() {
		if rerr != nil {
			panic(todo("", rerr))
		}
	}()
	dst = filepath.FromSlash(dst)
	src = filepath.FromSlash(src)
	si, err := os.Stat(src)
	if err != nil {
		if os.IsNotExist(err) && srcNotExistsOk {
			err = nil
		}
		return 0, 0, err
	}

	if !si.IsDir() {
		return 0, 0, fmt.Errorf("cannot copy a file: %s", src)
	}

	return files, bytes, filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.Mode()&os.ModeSymlink != 0 {
			target, err := filepath.EvalSymlinks(path)
			if err != nil {
				return fmt.Errorf("cannot evaluate symlink %s: %v", path, err)
			}

			if info, err = os.Stat(target); err != nil {
				return fmt.Errorf("cannot stat %s: %v", target, err)
			}

			if info.IsDir() {
				rel, err := filepath.Rel(src, path)
				if err != nil {
					return err
				}

				dst2 := filepath.Join(dst, rel)
				if err := os.MkdirAll(dst2, 0770); err != nil {
					return err
				}

				f, b, err := copyDir(dst2, target, canOverwrite, srcNotExistsOk)
				files += f
				bytes += b
				return err
			}

			path = target
		}

		rel, err := filepath.Rel(src, path)
		if err != nil {
			return err
		}

		if info.IsDir() {
			return os.MkdirAll(filepath.Join(dst, rel), 0770)
		}

		n, err := copyFile(filepath.Join(dst, rel), path, canOverwrite)
		if err != nil {
			return err
		}

		files++
		bytes += n
		return nil
	})
}

func mustCopyFile(dst, src string, canOverwrite func(fn string, fi os.FileInfo) bool) int64 {
	n, err := copyFile(dst, src, canOverwrite)
	if err != nil {
		fail(1, "%s\n", err)
	}

	return n
}

func copyFile(dst, src string, canOverwrite func(fn string, fi os.FileInfo) bool) (n int64, rerr error) {
	defer func() {
		if rerr != nil {
			panic(todo("", rerr))
		}
	}()
	src = filepath.FromSlash(src)
	si, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if si.IsDir() {
		return 0, fmt.Errorf("cannot copy a directory: %s", src)
	}

	dst = filepath.FromSlash(dst)
	dstDir := filepath.Dir(dst)
	di, err := os.Stat(dstDir)
	switch {
	case err != nil:
		if !os.IsNotExist(err) {
			return 0, err
		}

		if err := os.MkdirAll(dstDir, 0770); err != nil {
			return 0, err
		}
	case err == nil:
		if !di.IsDir() {
			return 0, fmt.Errorf("cannot create directory, file exists: %s", dst)
		}
	}

	di, err = os.Stat(dst)
	switch {
	case err != nil && !os.IsNotExist(err):
		return 0, err
	case err == nil:
		if di.IsDir() {
			return 0, fmt.Errorf("cannot overwite a directory: %s", dst)
		}

		if canOverwrite != nil && !canOverwrite(dst, di) {
			return 0, fmt.Errorf("cannot overwite: %s", dst)
		}
	}

	s, err := os.Open(src)
	if err != nil {
		return 0, err
	}

	defer s.Close()
	r := bufio.NewReader(s)

	d, err := os.Create(dst)

	defer func() {
		if err := d.Close(); err != nil && rerr == nil {
			rerr = err
			return
		}

		if err := os.Chmod(dst, si.Mode()); err != nil && rerr == nil {
			rerr = err
			return
		}

		if err := os.Chtimes(dst, si.ModTime(), si.ModTime()); err != nil && rerr == nil {
			rerr = err
			return
		}
	}()

	w := bufio.NewWriter(d)

	defer func() {
		if err := w.Flush(); err != nil && rerr == nil {
			rerr = err
		}
	}()

	return io.Copy(w, r)
}

func env(name, deflt string) (r string) {
	r = deflt
	if s := os.Getenv(name); s != "" {
		r = s
	}
	return r
}

// origin returns caller's short position, skipping skip frames.
//
//lint:ignore U1000 debug helper
func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
		if strings.HasPrefix(fns, "func") {
			num := true
			for _, c := range fns[len("func"):] {
				if c < '0' || c > '9' {
					num = false
					break
				}
			}
			if num {
				return origin(skip + 2)
			}
		}
	}
	return fmt.Sprintf("%s:%d:%s", filepath.Base(fn), fl, fns)
}

// todo prints and return caller's position and an optional message tagged with TODO. Output goes to stderr.
//
//lint:ignore U1000 debug helper
func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s\n\tTODO %s", origin(2), s)
	// fmt.Fprintf(os.Stderr, "%s\n", r)
	// os.Stdout.Sync()
	return r
}

// trc prints and return caller's position and an optional message tagged with TRC. Output goes to stderr.
//
//lint:ignore U1000 debug helper
func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stderr, "%s\n", r)
	os.Stderr.Sync()
	return r
}
