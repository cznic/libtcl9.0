// Copyright 2024 The libtcl9_0-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libtcl9_0 // import "modernc.org/libtcl9_0"

import (
	"flag"
	"fmt"
	"io"
	"os"
	goexec "os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"testing"
	"time"

	_ "github.com/adrg/xdg"
	_ "modernc.org/ccgo/v4/lib"
	_ "modernc.org/fileutil/ccgo"
	"modernc.org/libc"
	_ "modernc.org/libz"
)

var (
	goos     = runtime.GOOS
	goarch   = runtime.GOARCH
	target   = fmt.Sprintf("%s/%s", goos, goarch)
	notFiles = []string{}
	skip     []string

	// https://www.tcl.tk/man/tcl8.6/TclCmd/tcltest.html
	oDebug      = flag.String("debug", "0", "0, 1, 2 or 3")
	oFile       = flag.String("file", "", "pattern list")
	oMatch      = flag.String("match", "", "pattern list")
	oNotFile    = flag.String("notfile", strings.Join(notFiles, " "), "pattern list")
	oSingleProc = flag.String("singleproc", "0", "0 or 1")
	oSkip       = flag.String("xskip", "", "comma separated pattern list")
	oTmpdir     = flag.String("tmpdir", "", "directory")
	oVerbose    = flag.String("verbose", "el", "any combination of letters b, p, s, t, e, l, m, u")
	oXTags      = flag.String("xtags", "", "passed to go build of tcltest")
)

func init() {
	switch target {
	case "linux/386":
		skip = []string{
			//TODO investigate
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"cmdIL-5.7",
			"http11-2.10.1.1",
			"iortrans-3.1",
			"platform-3.1",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
			"unixInit-3.1",
		}
	case "linux/arm":
		skip = []string{
			//TODO investigate
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"chan-15.2",
			"iortrans-3.1",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
		}
	case "linux/s390x":
		notFiles = []string{
			"zipfs.test",
		}
		skip = []string{
			//TODO investigate
			"case-1.*",
			"case-2.*",
			"case-3.*",
			"clock-2.*",
			"clock-29.*",
			"clock-30.*",
			"clock-34.*",
			"clock-38.*",
			"clock-39.*",
			"clock-4.*",
			"clock-44.*",
			"clock-46.*",
			"clock-5.*",
			"clock-50.*",
			"clock-51.*",
			"clock-52.*",
			"clock-56.*",
			"clock-59.*",
			"clock-6.*",
			"clock-64.*",
			"clock-68.*",
			"clock-7.*",
			"clock-9.*",
			"cmdIL-5.7",
			"iortrans-3.1",
			"lseq-1.27",
			"socket_inet6-2.13",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
			"unixInit-3.1",
		}
	case "linux/riscv64":
		skip = []string{
			//TODO investigate
			"binary-40.3",
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"cmdIL-5.7",
			"httpProxy-2.1.0",
			"httpProxy-2.1.1",
			"httpProxy-3.1.0",
			"httpProxy-3.1.1",
			"httpProxy-4.1.0",
			"httpProxy-4.1.1",
			"httpProxy-5.1.0",
			"httpProxy-5.1.1",
			"iortrans-3.1",
			"lseq-1.27",
			"socket_inet6-2.13",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
			"unixInit-3.1",
		}
	case "linux/ppc64le":
		skip = []string{
			//TODO investigate
			"binary-80.*",
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"cmdIL-5.7 ",
			"iortrans-3.1",
			"lseq-1.27",
			"stringComp-1.1",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.3",
			"stringComp-8.4",
			"stringComp-8.4",
			"tcltest-21.11",
			"tcltest-21.11",
			"tcltest-21.5",
			"tcltest-21.5",
			"unixInit-3.1",
			"unixInit-3.1",
		}
	case "linux/amd64":
		skip = []string{
			// won't fix
			"cmdIL-5.7 ",          // fails on builder only
			"lseq-bug-54329e39c7", // fails on builder only

			//TODO investigate
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"iortrans-3.1",
			"platform-3.1",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
			"unixInit-3.1",
		}
	case "linux/arm64":
		skip = []string{
			//TODO investigate
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"iortrans-3.1",
			"lseq-1.27",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
			"tcltest-21.11",
			"tcltest-21.5",
		}
	case "linux/loong64":
		notFiles = []string{
			"httpProxy.test",
		}
		skip = []string{
			//TODO investigate
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"cmdIL-5.7",
			"iortrans-3.1",
			"iortrans-3.1",
			"stringComp-1.1",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.3",
			"stringComp-8.4",
			"stringComp-8.4",
			"tcltest-21.11",
			"tcltest-21.11",
			"tcltest-21.5",
			"tcltest-21.5",
			"unixInit-3.1",
			"unixInit-3.1",
		}
	case "darwin/amd64":
		notFiles = []string{
			"aaa_exit.test",
			"basic.test",
			"chan.test",
			"chanio.test",
			"cmdAH.test",
			"coroutine.test",
			"dict.test",
			"encoding.test",
			"event.test",
			"fCmd.test",
			"fileSystemEncoding.test",
			"http.test",
			"http11.test",
			"httpProxy.test",
			"interp.test",
			"io.test",
			"ioCmd.test",
			"ioTrans.test",
			"main.test",
			"msgcat.test",
			"notify.test",
			"oo.test",
			"process.test",
			"reg.test",
			"safe-zipfs.test",
			"set-old.test",
			"socket.test",
			"tcltest.test",
			"timer.test",
			"trace.test",
			"var.test",
			"zipfs.test",
			"zlib.test",
		}
		skip = []string{
			//TODO investigate
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"expr-54.2",
			"expr-54.3",
			"expr-54.4",
			"expr-54.5",
			"expr-55.2",
			"expr-55.3",
			"expr-55.4",
			"expr-55.5",
			"expr-55.7",
			"expr-55.8",
			"expr-56.7",
			"expr-56.8",
			"expr-58.6",
			"expr-59.2",
			"expr-59.3",
			"expr-59.4",
			"expr-59.5",
			"expr-59.6",
			"expr-59.7",
			"expr-59.8",
			"expr-61.14",
			"expr-61.15",
			"expr-61.22",
			"cmdMZ-6.5a",
			"expr-61.23",
			"expr-61.30",
			"expr-61.31",
			"expr-61.38",
			"expr-61.39",
			"expr-61.41",
			"expr-61.42",
			"expr-61.43",
			"expr-61.44",
			"expr-61.45",
			"expr-61.46",
			"expr-61.47",
			"expr-61.49",
			"expr-61.50",
			"expr-61.51",
			"expr-61.52",
			"expr-61.53",
			"expr-61.54",
			"expr-61.55",
			"expr-61.6",
			"expr-61.7",
			"filesystem-1.30.3",
			"lseq-1.27",
			"safe-16.2",
			"safe-16.7",
			"safe-16.8",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
			"unixInit-1.2",
		}
	case "darwin/arm64":
		notFiles = []string{
			"aaa_exit.test",
			"basic.test",
			"chan.test",
			"chanio.test",
			"cmdAH.test",
			"coroutine.test",
			"dict.test",
			"encoding.test",
			"event.test",
			"fCmd.test",
			"fileSystemEncoding.test",
			"http.test",
			"http11.test",
			"httpProxy.test",
			"interp.test",
			"io.test",
			"ioCmd.test",
			"ioTrans.test",
			"main.test",
			"msgcat.test",
			"notify.test",
			"oo.test",
			"process.test",
			"reg.test",
			"safe-zipfs.test",
			"set-old.test",
			"socket.test",
			"tcltest.test",
			"timer.test",
			"trace.test",
			"var.test",
			"zipfs.test",
			"zlib.test",
		}
		skip = []string{
			//TODO investigate
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"expr-54.2",
			"expr-54.3",
			"expr-54.4",
			"expr-54.5",
			"expr-55.2",
			"expr-55.3",
			"expr-55.4",
			"expr-55.5",
			"expr-55.7",
			"expr-55.8",
			"expr-56.7",
			"expr-56.8",
			"expr-58.6",
			"expr-59.2",
			"expr-59.3",
			"expr-59.4",
			"expr-59.5",
			"expr-59.6",
			"expr-59.7",
			"expr-59.8",
			"expr-61.14",
			"expr-61.15",
			"expr-61.22",
			"cmdMZ-6.5a",
			"expr-61.23",
			"expr-61.30",
			"expr-61.31",
			"expr-61.38",
			"expr-61.39",
			"expr-61.41",
			"expr-61.42",
			"expr-61.43",
			"expr-61.44",
			"expr-61.45",
			"expr-61.46",
			"expr-61.47",
			"expr-61.49",
			"expr-61.50",
			"expr-61.51",
			"expr-61.52",
			"expr-61.53",
			"expr-61.54",
			"expr-61.55",
			"expr-61.6",
			"expr-61.7",
			"filesystem-1.30.3",
			"lseq-1.27",
			"safe-16.2",
			"safe-16.7",
			"safe-16.8",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
			"unixInit-1.2",
		}
	case "windows/amd64":
		notFiles = []string{
			//TODO hangs
			"http11.test",

			//TODO crash
			"http.test",
			"socket.test",

			//TODO too many fails
			"winDde.test",
		}
		skip = []string{
			//TODO hangs
			"regexp-22.*",

			//TODO crash
			"chan-16.*",
			"chan-io-28.*",
			"chan-io-29.*",
			"chan-io-34.*",
			"chan-io-39.*",
			"chan-io-49.*",
			"chan-io-50.*",
			"chan-io-51.*",
			"chan-io-53.*",
			"chan-io-54.*",
			"chan-io-57.*",
			"chan-io-58.*",
			"event-11.*",
			"httpProxy-2.*",
			"httpProxy-3.*",
			"httpProxy-4.*",
			"httpProxy-5.*",
			"io-29.*",
			"io-39.*",
			"io-49.*",
			"io-50.*",
			"io-51.*",
			"io-53.*",
			"io-54.*",
			"io-57.*",
			"io-58.*",
			"iocmd-8.*",
			"registry-3.*",
			"registry-5.*",
			"registry-6.*",
			"registry-7.*",
			"registry-8.*",
			"winNotify-3.*",
			"winNotify-7.*",
			"winNotify-8.*",
			"zlib-8.*",
			"zlib-9.*",
			"zlib-10.*",

			//TODO fails
			"Tcl_Main-5.10",
			"case-1.1",
			"case-1.2",
			"case-1.3",
			"case-1.4",
			"case-1.5",
			"case-1.6",
			"case-1.7",
			"case-2.1",
			"case-2.2",
			"case-2.3",
			"case-2.4",
			"case-2.5",
			"case-3.1",
			"case-3.2",
			"case-3.3",
			"clock-38.2.vm:0",
			"clock-38.2.vm:1",
			"clock-38.3fm.vm:0",
			"clock-38.3fm.vm:1",
			"clock-38.3sc.vm:0",
			"clock-38.3sc.vm:1",
			"clock-40.1.vm:0",
			"clock-40.1.vm:1",
			"clock-42.1.vm:0",
			"clock-42.1.vm:1",
			"clock-49.2.vm:0",
			"clock-49.2.vm:1",
			"encoding-24.2",
			"env-2.1",
			"env-2.2",
			"env-2.3",
			"env-2.4",
			"env-2.5",
			"env-3.1",
			"env-4.1",
			"env-4.3",
			"env-4.4",
			"env-4.5",
			"env-5.1",
			"env-5.3",
			"env-5.5",
			"env-9.0",
			"exit-1.2",
			"fCmd-28.8",
			"fCmd-31.4",
			"fCmd-32.3",
			"fCmd-9.14.3",
			"iortrans-3.1",
			"platform-3.1",
			"registry-11.1",
			"registry-11.2",
			"registry-5.10",
			"registry-5.11",
			"registry-5.12",
			"registry-5.13",
			"registry-5.14",
			"registry-5.3",
			"registry-5.4",
			"registry-5.5",
			"registry-5.6",
			"registry-5.7",
			"registry-5.8",
			"registry-5.9",
			"registry-6.10",
			"registry-6.11",
			"registry-6.12",
			"registry-6.13",
			"registry-6.14",
			"registry-6.15",
			"registry-6.16",
			"registry-6.17",
			"registry-6.18",
			"registry-6.20",
			"registry-6.21",
			"registry-6.3",
			"registry-6.4",
			"registry-6.5",
			"registry-6.6",
			"registry-6.7",
			"registry-6.8",
			"registry-6.9",
			"safe-17.2",
			"safe-17.3",
			"safe-17.4",
			"safe-8.5",
			"safe-8.6",
			"safe-8.7",
			"safe-stock-18.2",
			"safe-stock-18.4",
			"safe-zipfs-18.2",
			"safe-zipfs-18.4",
			"stringComp-1.1",
			"stringComp-5.17",
			"stringComp-5.18",
			"stringComp-5.4",
			"stringComp-8.1",
			"stringComp-8.2",
			"stringComp-8.3",
			"stringComp-8.4",
			"tcltest-21.11",
			"tcltest-21.5",
			"winFCmd-1.11",
			"winFCmd-1.20",
			"winFCmd-1.23",
			"winFCmd-1.27",
			"winFCmd-1.3",
			"winFCmd-1.4",
			"winTime-1.1",
			"winTime-1.2",
			"winpipe-4.2",
			"winpipe-4.3",
			"winpipe-4.4",
			"winpipe-4.5",
		}
	}
}

func TestMain(m *testing.M) {
	flag.Parse()
	if s := *oSkip; s != "" {
		skip = append(skip, strings.Split(s, ",")...)
	}
	if s := *oNotFile; s != "" {
		notFiles = append(notFiles, strings.Split(s, ",")...)
	}
	rc := m.Run()
	os.Exit(rc)
}

func mustCString(tls *libc.TLS, s string) (r uintptr) {
	r, err := libc.CString(s)
	if err != nil {
		panic(err)
	}

	return r
}

func TestExpr(t *testing.T) {
	tls := libc.NewTLS()

	defer tls.Close()

	in := XTcl_CreateInterp(tls)
	rc := XTcl_EvalEx(tls, in, mustCString(tls, "set a [expr 42*314]"), -1, 0)
	if rc != 0 {
		t.Fatal(rc)
	}

	s := libc.GoString(XTcl_GetString(tls, XTcl_GetObjResult(tls, in)))
	if s != "13188" {
		t.Fatalf("%q", s)
	}

	t.Logf("%v, %q", rc, s)
}

func TestEnv(t *testing.T) {
	tls := libc.NewTLS()

	defer tls.Close()

	in := XTcl_CreateInterp(tls)
	rc := XTcl_EvalEx(tls, in, mustCString(tls, "set a $env(PATH)"), -1, 0)
	if rc != 0 {
		t.Fatal(rc)
	}

	s := libc.GoString(XTcl_GetString(tls, XTcl_GetObjResult(tls, in)))
	if s == "" {
		t.Fatalf("%q", s)
	}

	t.Logf("%v, '%s'", rc, s)
}

func Test2(t *testing.T) {
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	if wd, err = filepath.Abs(wd); err != nil {
		t.Fatal(err)
	}

	const lib = "TCL_LIBRARY"
	sav := os.Getenv(lib)

	defer func() {
		os.Setenv(lib, sav)
	}()

	pth := filepath.Join(wd, "library")
	os.Setenv(lib, pth)
	t.Logf("%s=%s", lib, pth)

	dir := t.TempDir()
	bin := filepath.Join(dir, "tcltest")
	if goos == "windows" {
		bin += ".exe"
	}
	out, err := goexec.Command("go", "build", "-o", bin, "-tags="+*oXTags, "./"+filepath.Join("internal", "tcltest")).CombinedOutput()
	if err != nil {
		t.Fatalf("%s\nFAIL: %s", out, err)
	}
	var stdout, stderr strings.Builder
	args := []string{
		filepath.Join(wd, "internal", "tests", "all.tcl"),
		"-debug", *oDebug,
		"-errfile", "errfile",
		"-singleproc", *oSingleProc,
	}
	if s := *oVerbose; s != "" {
		args = append(args, "-verbose", s)
	}
	if s := *oFile; s != "" {
		args = append(args, "-file", s)
	}
	if s := *oMatch; s != "" {
		args = append(args, "-match", s)
	}
	if len(skip) != 0 {
		args = append(args, "-skip", strings.Join(skip, " "))
	}
	if len(notFiles) != 0 {
		args = append(args, "-notfile", strings.Join(notFiles, " "))
	}
	if s := *oTmpdir; s != "" {
		args = append(args, "-tmpdir", s)
	}
	t.Logf("%q %q", bin, args)
	cmd := goexec.Command(bin, args...)
	cmd.Dir = dir
	cmd.Stdout = io.MultiWriter(os.Stdout, &stdout)
	cmd.Stderr = io.MultiWriter(os.Stderr, &stderr)
	cmd.WaitDelay = 10 * time.Second
	if err := cmd.Run(); err != nil {
		t.Error(err)
	}
	stdout.WriteString("\n")
	stdout.WriteString(stderr.String())
	alls := stdout.String()
	all := strings.Split(alls, "\n")
	if !strings.Contains(alls, "Total") ||
		!strings.Contains(alls, "Passed") ||
		!strings.Contains(alls, "Skipped") ||
		!strings.Contains(alls, "Failed") {
		t.Errorf("final summary not detected (test crashed?)")
		return
	}
out:
	for i, v := range all {
		switch {
		case
			strings.Contains(v, "Test file error"),
			strings.Contains(v, "panic:"),
			strings.HasPrefix(v, "====") && strings.Contains(v, "FAILED"):

			t.Error(v)
		case strings.HasPrefix(v, "all.tcl:"):
			t.Logf("\n%s", strings.Join(all[i:], "\n"))
			break out
		}
	}
	errFile, err := os.ReadFile(filepath.Join(dir, "errfile"))
	if err == nil && len(errFile) != 0 {
		t.Errorf("FAIL\n%s", errFile)
	}
	// 9.0b1
	//
	// C
	// 				all.tcl:	Total	56655	Passed	50893	Skipped	5762	Failed	0
	// Go
	//
	// 2024-05-14 linux/amd64	all.tcl:	Total	55130	Passed	49452	Skipped	5678	Failed	0
	// 2024-05-15 linux/amd64	all.tcl:	Total	56949	Passed	51166	Skipped	5783	Failed	0

	// 9.0b2
	//
	// C
	//
	// 2024-05-22 linux/amd64	all.tcl:	Total	66569	Passed	60795	Skipped	5774	Failed	0
	//
	// Go
	//
	// 2024-05-22 linux/amd64	all.tcl:	Total	66863	Passed	61068	Skipped	5795	Failed	0

	// 9.0 rc0
	//
	// C
	//
	// 2024-09-11 linux/amd64	all.tcl:	Total	67359	Passed	61592	Skipped	5767	Failed	0
	//
	// Go
	//
	// 2024-09-11 linux/amd64	all.tcl:	Total	67653	Passed	61863	Skipped	5790	Failed	0
}
